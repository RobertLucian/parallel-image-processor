# Parallel Image Processor w/ MPI

## Intro

In this project for *"Algoritmi Paraleli"* course (in English *"Parallel Algorithms"*), the task is building a parallelizable program that takes as an input an image and spits out its blurred counterpart. This has to be done in both ways, both in parallel with MPI and sequentially so that it can be shown the overall speedup of the parallelized code.


## Development

To run this, you need to have the C/C++ library for MPI. To launch/build this application, I'm resorting to docker as I have found a docker image on the hub that has all the libraries/toolchain preinstalled on it. You can run a container having all the necessary with the following command:

```bash
docker container run -it --rm -v ~/Dropbox/Work/Repos/parallel-image-processor:/prog -w /prog nlknguyen/alpine-mpich
```

You have at your own disposal the following commands:

1. `make parallel` - builds the parallelized MPI program.
1. `make serial` - builds the sequential program for the same thing.
1. `make runp` - run the parallel program.
1. `make runs` - run the sequential program.
1. `make clean` - clean the artifacts.

## Implementation

First and foremost, we need a library to decode and encode images. One that seems to be doing its job good is [lodepng](https://github.com/lvandeve/lodepng). The only drawback of this library is that it's only capable of reading/writing PNG photos.

Having decided the library for reading/writing PNG photos, let's see how we're going to process the images. We went with a very simple topology: a ring one. The idea is that the root node sends the full image to its next node in the chain (the first slave node) and then after the slave node receives it, the very first thing that it does is sending the same image it has just received to the next slave node in its chain. This is done until it hits the very last slave node in the chain.

At the same time as the images are propagated throughout the chain, each slave node starts processing its current image and applies a blur effect on a very specific part of the image depending on its rank in the chain. The program requires an odd number of processes so that the image can be divided in an even number of components - also the minimum number of nodes is 3. By default, the image's height will always be divided in 2 and the width will be divided in  *(n - 1) / 2* parts. Basically, we're making sure the image is always divided in 2 rows and another number of columns so that each slave process gets one.

As each node finishes processing its individual part of the image, each one of them checks if there's a lower-ranking slave node that wants to send its processed image, it receives it if that's the case, it applies its own changes and then it sends the newly processed image to the next slave node. This again happens for every slave node in the chain sequentially until it hits the very last one, at which point that one sends the fully assembled image back to the master node. The master node receives it and saves it. That's the result we're looking for.

In the following diagram it can be seen a circular topology. In the following example, the number of processes is set to 5 of which 4 are used to process the 4 portions of the image.

![](statics/topology.PNG)

For blurring the image, we have opted for a box type of blur, which is the simplest of them all. The kernel size of the filter is set to 10.

## Results

During tests, the execution time for both the sequential and the parallel code is mostly the same with some times when the parallel one takes more time. The only moment when it's actually faster in parallel is when a very large image is processed - say something that has a dimension of anywhere above 100MB. 

Testing the blurring effect on an image of the universe with a size of 120MB takes roughly *350* seconds when it's run sequentially and about *195* seconds when it's in parallel. The latter one requires 7 processes (with the master node included), which doesn't make for much big of a speedup, but at least, there's an improvement to it.

Next up is a comparison of the effect of blur on the image with the universe. Notice that in this photo I really had to zoom a lot in order to see the blurring effect. That's because the image's resolution is so big that even a kernel size of 10 is nothing.

![](statics/universe_comparison.jpg)

Here's the actual image when it's zoomed out.

![](statics/universe.PNG)

To actually see in what order each part of the image is processed, check the following collage of 6 pictures. Each picture has yet another portion of the image blurred when compared to the previous one. There are 6 images, because there were 6 transitions, all due to the fact that program was launched with 7 processes (1 + 6). If it's hard to see the differences between each picture, then you'll have to squint even more.

![](statics/sequential_blur_face.jpg)

And here's the order of the processing process in each part of the image.

![](statics/face_steps.gif)