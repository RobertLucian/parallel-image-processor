#include <mpi.h>
#include <time.h>
#include <stdio.h>
#include <stdlib.h>
#include "lodepng.h"

typedef struct {
  unsigned char* image;
  unsigned width, height, channels;
} Image;


/*
Encode from raw pixels to an in-memory PNG file first, then write it to disk
The image argument has width * height RGBA pixels or width * height * 4 bytes
*/
void encodeTwoSteps(const char* filename, Image *image) {
  unsigned char* png;
  size_t pngsize;

  unsigned error = lodepng_encode32(&png, &pngsize, image->image, image->width, image->height);
  if(!error) lodepng_save_file(png, pngsize, filename);

  /*if there's an error, display it*/
  if(error) printf("error %u: %s\n", error, lodepng_error_text(error));

  free(png);
}

/*
Load PNG file from disk to memory first, then decode to raw pixels in memory.
*/
void decodeTwoSteps(const char* filename, Image *image) {
  unsigned error;
  unsigned char* png = 0;
  size_t pngsize;

  error = lodepng_load_file(&png, &pngsize, filename);
  if(!error) error = lodepng_decode32(&image->image, &image->width, &image->height, png, pngsize);
  image->channels = 4;
  if(error) printf("error %u: %s\n", error, lodepng_error_text(error));

  free(png);
}

void sendImage(Image *image, int to_whom) {
  MPI_Send(&image->height, 1, MPI_UNSIGNED, to_whom, 0, MPI_COMM_WORLD);
  MPI_Send(&image->width, 1, MPI_UNSIGNED, to_whom, 0, MPI_COMM_WORLD);
  MPI_Send(&image->channels, 1, MPI_UNSIGNED, to_whom, 0, MPI_COMM_WORLD);
  MPI_Send(image->image, image->height * image->width * image->channels, MPI_BYTE, to_whom, 0, MPI_COMM_WORLD);
}

Image receiveImage(int from_whom) {
  Image aux;
  MPI_Recv(&aux.height, 1, MPI_UNSIGNED, from_whom, 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
  MPI_Recv(&aux.width, 1, MPI_UNSIGNED, from_whom , 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
  MPI_Recv(&aux.channels, 1, MPI_UNSIGNED, from_whom, 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
  aux.image = (unsigned char*) malloc(aux.height * aux.width * aux.channels);
  MPI_Recv(aux.image, aux.height * aux.width * aux.channels, MPI_BYTE, from_whom, 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
  
  return aux;
}

void applyBlurOnPixel(Image *src, Image *dest, int current_row, int current_column, int current_chn) {
  int x, y;
  float kernel[] = {
    1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
    1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
    1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
    1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
    1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
    1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
    1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
    1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
    1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
    1, 1, 1, 1, 1, 1, 1, 1, 1, 1
  };
  int kernel_size = 10;
  int kernel_sum = 100;
  float aux = 0.0;
  for(x = 0; x < kernel_size; x++) {
    for(y = 0; y < kernel_size; y++) {
      int nx = current_row + x - 1;
      int ny = current_column + y - 1;
      if(nx >= 0 && nx < (int)src->height && ny >= 0 && ny < (int)src->width)
          aux += (float)(src->image[nx * src->width * src->channels + ny * src->channels + current_chn]) * (kernel[x * kernel_size + y]);
    }
  }
  aux /= kernel_sum;
  dest->image[current_row * dest->width * dest->channels + current_column * dest->channels + current_chn] = (unsigned char)(aux);
}

void copyImage(Image *src, Image *dest, int current_row, int current_column, int current_chn) {
  int index = current_row * src->width * src->channels + current_column * src->channels + current_chn;
  dest->image[index] = src->image[index]; 
}

void processEachPixel(Image *src, Image *dest, int row_start, int row_end, int column_start, int column_end, int chn_start, int chn_end, void (*func)(Image*, Image*, int, int, int)) {
  int i, j, k;
  for(i = row_start; i < row_end; i++) {
    for(j = column_start; j < column_end; j++) {
      for(k = chn_start; k < chn_end; k++) {
        func(src, dest, i, j, k);
      }
    }
  }
}

int main() {
    
    /*Initialize the MPI environment*/
    MPI_Init(NULL, NULL);

    /*Get the number of processes*/
    int world_size;
    MPI_Comm_size(MPI_COMM_WORLD, &world_size);

    /*Get the rank of the process*/
    int world_rank;
    MPI_Comm_rank(MPI_COMM_WORLD, &world_rank);

    /*Get the name of the processor*/
    char processor_name[MPI_MAX_PROCESSOR_NAME];
    int name_len;
    MPI_Get_processor_name(processor_name, &name_len);

    if(world_size % 2 == 1) {
      
      // if we've got the root node
      if(world_rank == 0) { 
        // start measuring time
        float start = clock() * 1000 / CLOCKS_PER_SEC;

        // read image from file
        Image image;
        decodeTwoSteps("images/universe.png", &image);
        // and then send it to the next slave node
        sendImage(&image, world_rank + 1);

        // and finally receive the processed image from the last node
        free(image.image);
        image = receiveImage(world_size - 1);

        // save the blurred image
        encodeTwoSteps("generated/universe_out.png", &image);
        free(image.image);

        // and print the execution time
        float end = clock() * 1000 / CLOCKS_PER_SEC;
        float t = (end - start) / 1000;  
        printf("Execution time: %.2f seconds\n", t);

      // for all other slave nodes
      } else if (world_rank < world_size) {
          // receive image from the previous node
          Image image  = receiveImage(world_rank - 1);
          
          // and if there are nodes left in the chain
          // send them the pictures too
          if(world_rank < world_size - 1) {
            sendImage(&image, world_rank + 1);
          }

          // calculate the current row/column/sizes
          int dials = world_size - 1;
          int row = world_rank > (dials / 2);
          int column = (world_rank - 1) % (dials / 2);
          int row_size = image.height / 2;
          int column_size = 2 * image.width / dials;

          Image processed = image;
          processed.image = (unsigned char*) malloc(processed.height * processed.width * processed.channels);
          
          // apply blur on image section and save it in processed
          processEachPixel(&image, &processed, row * row_size, (row + 1) * row_size, column * column_size, (column + 1) * column_size, 0, image.channels, applyBlurOnPixel);
          
          // if we have an earlier processed image, receive it and save it
          if(world_rank > 1) {
            free(image.image);
            image = receiveImage(world_rank - 1);
          } 
          // and then copy-paste image onto processed
          processEachPixel(&processed, &image, row * row_size, (row + 1) * row_size, column * column_size, (column + 1) * column_size, 0, image.channels, copyImage);

          // save each stage
          /*
          char buffer[32];
          sprintf(buffer, "generated/face_%d.png", world_rank);
          encodeTwoSteps(buffer, &processed);
          */

          // if there are still nodes that need to get processed images then send it
          if(world_rank > 0 && world_rank < world_size - 1) {
            sendImage(&image, world_rank + 1);
          }
          
          // if it's the last node in the chain, send it to the master node
          if(world_rank == world_size - 1) {
            sendImage(&image, 0);
          }

          // release memory
          free(image.image);
          free(processed.image);
        }
      }

    /*Finalize the MPI environment*/
    MPI_Finalize();

    return 0;
}
