#include <stdio.h>
#include <stdlib.h>
#include "lodepng.h"
#include <time.h>

typedef struct {
  unsigned char* image;
  unsigned width, height, channels;
} Image;

void encodeTwoSteps(const char* filename, Image *image) {
  unsigned char* png;
  size_t pngsize;

  unsigned error = lodepng_encode32(&png, &pngsize, image->image, image->width, image->height);
  if(!error) lodepng_save_file(png, pngsize, filename);

  /*if there's an error, display it*/
  if(error) printf("error %u: %s\n", error, lodepng_error_text(error));

  free(png);
}

void decodeTwoSteps(const char* filename, Image *image) {
  unsigned error;
  unsigned char* png = 0;
  size_t pngsize;

  error = lodepng_load_file(&png, &pngsize, filename);
  if(!error) error = lodepng_decode32(&image->image, &image->width, &image->height, png, pngsize);
  image->channels = 4;
  if(error) printf("error %u: %s\n", error, lodepng_error_text(error));

  free(png);
}

void applyBlurOnPixel(Image *src, Image *dest, int current_row, int current_column, int current_chn) {
  int x, y;
  float kernel[] = {
    1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
    1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
    1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
    1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
    1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
    1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
    1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
    1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
    1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
    1, 1, 1, 1, 1, 1, 1, 1, 1, 1
  };
  int kernel_size = 10;
  int kernel_sum = 100;
  float aux = 0.0;
  for(x = 0; x < kernel_size; x++) {
    for(y = 0; y < kernel_size; y++) {
      int nx = current_row + x - 1;
      int ny = current_column + y - 1;
      if(nx >= 0 && nx < (int)src->height && ny >= 0 && ny < (int)src->width)
          aux += (float)(src->image[nx * src->width * src->channels + ny * src->channels + current_chn]) * (kernel[x * kernel_size + y]);
    }
  }
  aux /= kernel_sum;
  dest->image[current_row * dest->width * dest->channels + current_column * dest->channels + current_chn] = (unsigned char)(aux);
}

void copyImage(Image *src, Image *dest, int current_row, int current_column, int current_chn) {
  int index = current_row * src->width * src->channels + current_column * src->channels + current_chn;
  dest->image[index] = src->image[index]; 
}

void processEachPixel(Image *src, Image *dest, int row_start, int row_end, int column_start, int column_end, int chn_start, int chn_end, void (*func)(Image*, Image*, int, int, int)) {
  int i, j, k;
  for(i = row_start; i < row_end; i++) {
    for(j = column_start; j < column_end; j++) {
      for(k = chn_start; k < chn_end; k++) {
        func(src, dest, i, j, k);
      }
    }
  }
}

int main() {
    // start measuring time
    float start = clock() * 1000 / CLOCKS_PER_SEC;

    // read the image from file & 
    // initialize a new pool of memory
    Image image, output;
    decodeTwoSteps("images/universe.png", &image);
    output = image;
    output.image = (unsigned char*) malloc(image.width * image.height * image.channels);

    // blur the image
    processEachPixel(&image, &output, 0, image.height, 0, image.width, 0, image.channels, applyBlurOnPixel);
    
    // save the blurred image
    encodeTwoSteps("generated/universe_out.png", &output);
    free(image.image); 
    free(output.image);

    // print execution time
    float end = clock() * 1000 / CLOCKS_PER_SEC;
    float t = (end - start) / 1000;   
    printf("Execution time: %.2f seconds\n", t);
}