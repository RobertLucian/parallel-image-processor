EXECS1=parallel_image_processor
EXECS2=serial_image_processor
MPICC?=mpicc
GCC?=gcc

parallel: main.c
	${MPICC} -o parallel_image_processor -Wall -Wextra -pedantic -ansi -O3 main.c -std=gnu11 lodepng.c

serial: serialized.c
	${GCC} -o serial_image_processor -Wall -Wextra -pedantic -ansi -O3 serialized.c -std=gnu11 lodepng.c

runp:
	mpiexec -n 5 ./${EXECS1}

runs:
	./${EXECS2}
clean:
	rm -f ${EXECS1} ${EXECS2}